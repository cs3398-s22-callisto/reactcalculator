import React from "react";
import ReactDOM from "react-dom";
import ReactTestUtils from 'react-dom/test-utils'
import App from "../component/App";
import ButtonPanel from "../component/ButtonPanel"

// Creates a local html element "div" and renders the App component into it.
test("renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(<App />, div);
});

// Tests whether App's initial state properties are all null.
test('Is initial state null?', ()=>{
        let component = ReactTestUtils.renderIntoDocument(<App/>);
        let state = component.state;
        expect(state.total).toBe(null);
        expect(state.next).toBe(null);
        expect(state.operation).toBe(null);
}); 

test('Does Button Panel have any button tags', ()=>{
        const component = ReactTestUtils.renderIntoDocument(<ButtonPanel/>);
        let div = ReactTestUtils.scryRenderedDOMComponentsWithTag(component,'button');   
       
}); 
// Bryce A18 Test - Tag Div in Application

it("div tag test", () => {
        const div = document.createElement("div");
        ReactDOM.render(<div />, div);
      });


