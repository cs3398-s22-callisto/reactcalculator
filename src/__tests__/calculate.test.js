import React from 'react';
import calculate from "../logic/calculate";


describe('Calculate Tests', ()=>{
    test('Number button tests', () => {
        expect(calculate({
                total: null,
                next: null,
                operation: null},"4")).toHaveProperty('next', "4");
        expect(calculate({
                total: null,
                next: 1,
                operation: null},"1")).toHaveProperty('next', "11");
        expect(calculate({
                total: null,
                next: null,
                operation: "+"},"1")).toHaveProperty('next', "1");
    });
    

    test('Addition button tests', () => {
        expect(calculate({
                total: null,
                next: 1,
                operation: null},"+")).toHaveProperty('next', null);

    });

    // Bryce A18 Test - Addition Button Test
    // total = previous , next = current
    test('Addition button tests', () => {
        expect(calculate({
                total: null,
                next: 2,
                operation: null},"+")).toHaveProperty('next', null);

    });

    // Bryce A18 Test - Minus Button Test
    // total = previous , next = current
    test('Subtraction button test', () => {
        expect(calculate({
                total: null,
                next: 1,
                operation: null},"-")).toHaveProperty('next', null);

    });
    // Bryce A18 Test - % (Percent) button test
    // total = previous , next = current 
    test('Percent button test', () => {
        expect(calculate({
                total: null,
                next: 1,
                operation: null},"%")).toHaveProperty('next', "0.01");
    });
    
    // Bryce A18 Test - .(Decimal Point) button test
    // total = previous , next = current 
    test('Decimal Point button test', () => {
        expect(calculate({
                total: null,
                next: '.',
                operation: null},2)).toHaveProperty('next', ".2");

    });



    test('Equal button tests', () => {
        expect(calculate({
                total: 2,
                next: 2,
                operation: "+"},"=")).toHaveProperty('total', "4");
        expect(calculate({
                total: 2,
                next: 21,
                operation: "+"},"=")).toHaveProperty('total', "23");
    });
});


